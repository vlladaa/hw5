const body = document.querySelector('body')
const theme = document.querySelector('.theme')


theme.addEventListener('click', () => {
  
  body.classList.toggle('light')
  body.classList.toggle('standart')
  
  if (body.classList.contains('standart')) {
    localStorage.setItem('theme', 'standart');
  } else {
    localStorage.setItem('theme', 'light');
  }
  }
)

window.addEventListener('load', () => {
  const savedTheme = localStorage.getItem('theme');
  if (savedTheme) {
    body.classList.remove('light', 'standart');
    body.classList.add(savedTheme);
  }
});